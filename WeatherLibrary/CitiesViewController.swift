//
//  CitiesViewController.swift
//  WeatherLibrary
//
//  Created by Администратор on 07/10/2019.
//  Copyright © 2019 Sergey Klimovich. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    //Ссылка на таблтцу
    @IBOutlet weak var cityTable: UITableView!
    
    //Массив с городами
    var cityArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Подписываемся на делегат (действия в таблице) и datasourse заполнение таблицы
        cityTable.delegate = self
        cityTable.dataSource = self
        // Do any additional setup after loading the view.
    }
    //Колличество ячеек
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Количество элементов в массиве
        return cityArray.count
    }
    //Отрисовка ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Создаем переменную с ячейкой. Важно указать свой идентификатор ячейки из сториборда
        let cell = cityTable.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
        //Присваиваем текст стандартному Label из массива по индексу
        cell.textLabel?.text = cityArray[indexPath.row]
        return cell
    }

    
   
    
    
    @IBAction func addnewCity(_ sender: Any) {
        //Создание контроллера диалогового окна (alert)
        let alert = UIAlertController(title: "Добавление города", message: "Введите пожалуйста Ваш город", preferredStyle: .alert)
        //Добавление поля для ввода города
        alert.addTextField { textField in
            textField.placeholder = "City"
        }
        //Создание действия ок с добавлением в массив содержимого поля для ввода
        let confirmAction = UIAlertAction(title: "OK", style: .default) { [weak alert] _ in
            //Первое поле для ввода
            guard let textField = alert?.textFields?.first else { return }
            //Добавление в массив города
            self.cityArray.append(textField.text!)
            //Обновление таблицы
            self.cityTable.reloadData()
        }
        //Создание действия отмена
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        //Добавление действий
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        //Добавление на экран
        present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.cityArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.cityTable.reloadData()
        }
    }
    
    // MARK: - Navigation

       // In a storyboard-based application, you will often want to do a little preparation before navigation
    //Метод перехода
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Нажатая ячейка
        let selectCell = cityTable.indexPathForSelectedRow
        //Класс назначения (куда переходим)
        let vc = segue.destination as! ViewController
        //Присваиваем значение переменной в удаленном классе
        vc.city = cityArray[selectCell!.row]
       }

}
