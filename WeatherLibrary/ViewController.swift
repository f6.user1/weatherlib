//
//  ViewController.swift
//  WeatherLibrary
//
//  Created by Администратор on 07/10/2019.
//  Copyright © 2019 Sergey Klimovich. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cityLabel: UILabel!
    
    var city = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if city != "" {
            //Ccылка на api
             let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&apiKey=6da8e8bdb22e1d408ffb437eab399b45"
            let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            //Запрос через библиотеку
            Alamofire.request(url!, method: .get).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    self.cityLabel.text = self.city + " " + json["main"]["temp"].stringValue
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
    
    @IBAction func action(_ sender: Any) {
        if city != "" {
            //Ccылка на api
             let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&apiKey=6da8e8bdb22e1d408ffb437eab399b45"
            let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            //Запрос через библиотеку
            Alamofire.request(url!, method: .get).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    self.cityLabel.text = self.cityTextField.text! + " " + json["main"]["temp"].stringValue
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
}

